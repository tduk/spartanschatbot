# Spartans chatbot

A chatbot initially created during Vodafone Hackathon in May 2019. It receives inputs from Slack, passes the inputs to DialogFlow to detect user intent, 
then responds to the user with Vodafone's services 

## Dependencies

 - python2.7
 - slackclient 1.3.1 (https://github.com/slackapi/python-slackclient)
 - dialogflow 0.6.0 (https://github.com/googleapis/dialogflow-python-client-v2/releases)

## Start application without Docker
 
 1. Environment variables may need updating in set_env.sh. Go to the project folder and run 
 ```
 $ . config/set_env.sh 
 ```
 2. Start the chatbot 
 ```
 python src/main.py
 ```
## Start application with Docker

### Create the docker for the chatbot (whenever updating code)
Go to the docker folder run
 ```
 bash create_docker.sh
 ```
### Run the docker
Go to the docker folder run
 ```
 bash pull_and_run.sh
 ```
## License

MIT