dockerImageName=triendo/spartanschatbot
dockerpid=`docker ps -a | grep $dockerImageName | grep "Up" | awk -F " " '{ print $1 }'`
if [[ $dockerpid != "" ]];then
   docker kill $dockerpid
fi
docker pull $dockerImageName
docker run -d  $dockerImageName