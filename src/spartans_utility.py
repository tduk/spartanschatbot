import spartans_services
def process_intent(dialogflow):
    intent = dialogflow["intent"]
    payload = dialogflow["payload"]
    if intent == '':
        return ["Sorry, I am not sure I understand what you say. Please try again!"]
    if intent == 'getMembers':
        return ['The Spartans at Vodafone UK are:', 'Stephen Donlon: Chapter Lead','Nick Fifield: Scum Master','Saleem Aubdool: Product Owner','Abdul Khaled: Software Engineer','David Osseo-asare: Software Engineer',
        'Gayathri Subbu: Software Engineer',"James D'Rozario: Software Engineer",'Sindhu Natarajan: Software Engineer','Stefano Cafarella: Software Engineer','Trien Do: Software Engineer']
    elif intent == 'getServices':
        return ['Services owned by Spartans are: ','Appointment', 'Basket', 'Compatibility', 'YouthOrder', 'Service-Availability', 'Premise']
    elif intent == 'Default Welcome Intent':
        return ["Hey there, I am part of Spartans at Vodafone. Currently I can answer questions about Spartans' members, services, status of services in dev/int/sit1, list of defects. How can I help you?"]
    elif intent == 'Default Fallback Intent':
        return ["Oops, I'm sorry I didn't get that"]
    elif intent == 'Status':
        response = spartans_services.healthCheck(payload)
        return response
    elif intent == 'BugState':
        response = spartans_services.defect(payload)
        return response
    elif intent == 'Goodbye':
        return ["Goodbye, hope I helped. Please vote for me :)"]