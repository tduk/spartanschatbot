import requests

notClearMessage = "Sorry, I am not sure I understand what you say. Please try again!"

def callApiWithAuthentication(url):
    return requests.get(url,headers={"Authorization": "Basic c2luZGh1Lm5hdGFqYXJqYW5Adm9kYWZvbmUuY29tOnRlM3RjZno1bjNrcmRnemJ6emNqZzVoamZ2bGliZ21oNm00Z2M0M3FwZW42ZmRvd2RnN2E="})

def healthCheck(payload):
    service = payload["service"].lower()
    environment = payload["environment"]
    if service == '' or environment == '':
        return [notClearMessage]
    url = 'https://dal.dx-{0}.internal.vodafoneaws.co.uk:10050/{1}/actuator/health'.format(environment,service)
    response = requests.get(url,verify=False)
    rs = 'The service {0} in {1} is {2}'.format(service,environment,response.json()["status"])
    return [rs]

def defect(payload):
    url = 'https://dev.azure.com/vfuk-digital/Digital/_apis/wit/wiql/22257760-5fb2-42e8-a2ac-fc397d688c66?api-version=5.0'
    response = callApiWithAuthentication(url)
    workItems = response.json()["workItems"]
    defectList = []
    for item in workItems:
        defect = {}
        defect["ID"] = item["id"]
        defect["URL"] = item["url"]
        rs = callApiWithAuthentication(item["url"]).json()
        defect["State"] = rs["fields"]["System.State"]
        defect["Title"] = rs["fields"]["System.Title"]
        try:
            assignedTo = rs["fields"]["System.AssignedTo"]["displayName"] 
        except:
            assignedTo = "N/A"
        defect["AssignedTo"] = assignedTo

        defectList.append(str(defect).replace("u'","'" ))
    return defectList